#include "tables.h"
#include "record_mgr.h"
#include <stdlib.h>
#include <limits.h>
#include <float.h>

int insertRecordQuick(int id, char *name, bool late, RM_TableData * rel, Schema * schema);
int main(void){
  RM_TableData *rel = (RM_TableData *) malloc(sizeof(RM_TableData));
  RM_ScanHandle *sc = (RM_ScanHandle *) malloc(sizeof(RM_ScanHandle));
  int rc;

  initRecordManager(NULL);
  int numAttr = 3;
  char * attrNames[3] = {"ID", "Name", "Late"};
  DataType dataTypes[3] = {DT_INT, DT_STRING, DT_BOOL};
  int typeLength[3] = {4, 20, 1};
  int keySize = 1;
  int keys[1] = {0};
  Schema* schema = createSchema (numAttr, attrNames, dataTypes, typeLength, keySize, keys);

  createTable("R", schema);
  openTable(rel, "R");

  insertRecordQuick(0, "Jelani", true, rel, schema);
  insertRecordQuick(1, "Adeena", true, rel, schema);
  insertRecordQuick(2, "David", false, rel, schema);

  startScan(rel, sc, NULL);
  Record * r = NULL;
  createRecord(&r, schema);
  while((rc = next(sc, r)) == RC_OK) 
    {
      printf("Record\n RID\n page:%i, sector:%i\n data:%s\n", r->id.page, r->id.slot, r->data);
    }
    if (rc != RC_RM_NO_MORE_TUPLES)
       printf("%d\n", rc);
 
  closeScan(sc);	
 	
  return 0;
}
int insertRecordQuick(int id, char * name, bool late, RM_TableData * rel, Schema * schema){
  Record * record = (Record *)NULL;
  Value * value = malloc(sizeof(Value));
  createRecord(&record, schema);
  int offset = 0;
  
  value->dt = DT_INT;
  value->v.intV = id;
  setAttr(record, schema, 0, value, &offset);
  
  value->dt = DT_STRING;
  value->v.stringV = name;
  setAttr(record, schema, 1, value, &offset);
    
  value->dt = DT_BOOL;
  value->v.boolV = late;
  setAttr(record, schema, 2, value, &offset);

  insertRecord(rel, record);

  free(value);

  return 0;
}
