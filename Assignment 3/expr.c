#include "expr.h"
#include "record_mgr.h"
#include <string.h>
#include <stdlib.h>

//expression evaluation methods
extern RC valueEquals (Value *left, Value *right, Value *result){
	if(left->dt != right->dt)
		return RC_INVALID_EXPRESSION;
	
	result->dt = DT_BOOL;

	switch(left->dt){
		case DT_INT:
			result->v.boolV = (left->v.intV == right->v.intV);
			break;
		case DT_FLOAT:
                        result->v.boolV = (left->v.floatV == right->v.floatV);
			break;
		case DT_BOOL:
                        result->v.boolV = (left->v.boolV == right->v.boolV);
			break;
		case DT_STRING:
                        result->v.boolV = strcmp(left->v.stringV, right->v.stringV) == 0;
			break;
	}
	return RC_OK;
}
extern RC valueSmaller (Value *left, Value *right, Value *result){
	if(left->dt != right->dt)
                return RC_INVALID_EXPRESSION;

        result->dt = DT_BOOL;

        switch(left->dt){ 
                case DT_INT:
                        result->v.boolV = (left->v.intV < right->v.intV);
			break;
                case DT_FLOAT:
                        result->v.boolV = (left->v.floatV < right->v.floatV);
			break;
                case DT_BOOL:
                        result->v.boolV = (left->v.boolV < right->v.boolV);
			break;
                case DT_STRING:
                        result->v.boolV = strcmp(left->v.stringV, right->v.stringV) < 0;
			break;
        }
        return RC_OK;
}
extern RC boolNot (Value *input, Value *result){
	if(input->dt != DT_BOOL)
		return RC_INVALID_EXPRESSION;
	
	result->dt = DT_BOOL;
	result->v.boolV = !(input->v.boolV);

	return RC_OK;
}
extern RC boolAnd (Value *left, Value *right, Value *result){
	if(left->dt!=DT_BOOL || right->dt!=DT_BOOL)
		return RC_INVALID_EXPRESSION;

	result->dt = DT_BOOL;
	result->v.boolV = left->v.boolV && right->v.boolV;

	return RC_OK;
}
extern RC boolOr (Value *left, Value *right, Value *result){
	if(left->dt!=DT_BOOL || right->dt!=DT_BOOL)
                return RC_INVALID_EXPRESSION;

        result->dt = DT_BOOL;
        result->v.boolV = left->v.boolV || right->v.boolV;

        return RC_OK;

}
extern RC evalExpr (Record *record, Schema *schema, Expr *expr, Value **result){
	Value * leftInput;
	Value * rightInput;
	MAKE_VALUE(*result, DT_INT, -1);

	switch (expr->type){
		case EXPR_OP:{
			Operator * op = expr->expr.op;
			bool twoArgs = (op->type != OP_BOOL_NOT);

			evalExpr(record, schema, op->args[0], &leftInput);
			if(twoArgs)
				evalExpr(record, schema, op->args[1], &rightInput);
			switch(op->type){
				case OP_BOOL_NOT: 
					boolNot(leftInput, *result);
					break;
				case OP_BOOL_AND:
					boolAnd(leftInput, rightInput, *result);
					break;
				case OP_BOOL_OR:
					boolOr(leftInput, rightInput, *result);
					break;
				case OP_COMP_EQUAL:
					valueEquals(leftInput, rightInput, *result);
					break;
				case OP_COMP_SMALLER:
					valueSmaller(leftInput, rightInput, *result);
					break;
			}
			free(leftInput);
			if(twoArgs)
				free(rightInput);
		}
		case EXPR_CONST:
			CPVAL(*result, expr->expr.cons);
			break;
		case EXPR_ATTRREF:
			free(*result);
			int offset[] = {1};
			getAttr(record,schema, expr->expr.attrRef, result, offset);
			break;
	}
}
extern RC freeExpr (Expr *expr){
	switch(expr->type){
		case EXPR_OP:{
			Operator * op = expr->expr.op;
			switch(op->type){
				case OP_BOOL_NOT:
					free(op->args[0]);
					break;
				default:
					free(op->args[0]);
					free(op->args[1]);
					break;
			}
			free(op->args);
		}
		case EXPR_CONST:
			freeVal(expr->expr.cons);
			break;
		case EXPR_ATTRREF:
			break;
	}
	free(expr);
}
extern void freeVal(Value *val){
	if(val->dt == DT_STRING)
		free(val->v.stringV);
	free(val);
}
