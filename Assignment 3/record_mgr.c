#include "dberror.h"
#include "expr.h"
#include "tables.h"
#include "record_mgr.h"
#include <string.h>
#include <stdlib.h>

RM_TableList * list;
// table and manager
RC searchTable(RM_TableData * table, char *name){
	if(list->numberOfTables == 0)
		return RC_TABLE_NOT_FOUND;
	RM_TableData * current = list->head;
	while(current != list->tail && strcmp(current->name,name)!=0)
		current = current->next;
	if(current == list->tail && strcmp(current->name,name)!=0)
		return RC_TABLE_NOT_FOUND;
        table->name = current->name;
        table->schema = current->schema;
        table->numberOfTuples = current->numberOfTuples;
        table->mgmtData = current->mgmtData;
	return RC_OK;
} 
RC searchTableP(RM_TableData ** table, char *name){
	if(list->numberOfTables == 0)
                return RC_TABLE_NOT_FOUND;
        RM_TableData * current = list->head;
        while(current != list->tail && strcmp(current->name,name)!=0)
                current = current->next;
        if(current == list->tail && strcmp(current->name,name)!=0)
                return RC_TABLE_NOT_FOUND;
        *table = current;
        return RC_OK;

}
RC appendTable(RM_TableData * table){
	if(list->numberOfTables == 0){
		list->head = table;
		list->tail = table;
		table->next = table;
		table->previous = table;
	}
	else{
		table->previous = list->tail;
		table->next = list->head;
		list->tail->next = table;
		list->head->previous = table;
		list->tail = table;
	}
	list->numberOfTables++;
	return RC_OK;
}
RC removeTable(RM_TableData * table){
	table->previous->next = table->next;
	table->next->previous = table->previous;
	if(table == list->head)
		list->head = table->next;
	else if(table == list->tail)
		list->tail = table->previous;
	list->numberOfTables++;
	return RC_OK; 
}
extern RC initRecordManager (void *mgmtData){
	//Expecting mgmtData to be of type RM_TableList **
	initStorageManager();
	list = calloc(1,sizeof(RM_TableList));
		 
}
extern RC shutdownRecordManager (){
	free(list);
	//free all the info inside of tables and then free tables
}
extern RC createTable (char *name, Schema *schema){
	//Populate table
	RM_TableData * table = (RM_TableData *)malloc(sizeof(RM_TableData));
	table->name = name;
	table->schema = schema;
	table->numberOfTuples = 0;
	table->mgmtData = NULL;
	
	//Add to table list
	appendTable(table);
	
	//Create page file for table
	RC rc = createPageFile(name);
	if(rc!=RC_OK)
		return rc;

	return RC_OK;
}
extern RC openTable (RM_TableData *rel, char *name){
	RC rc = searchTable(rel, name);
	if(rc!= RC_OK)
		return rc;

	BM_BufferPool * bm = MAKE_POOL();

	rc = initBufferPool(bm, name, 3, RS_LRU, NULL);
	if(rc!=RC_OK)
		return rc;

	rel->mgmtData = bm;
	return RC_OK;
}
extern RC closeTable (RM_TableData *rel){
	//Assuming that search is not needed
	RC rc = shutdownBufferPool(rel->mgmtData);
	if(rc != RC_OK)
		return rc;

	free(rel->mgmtData);
	return RC_OK;
}
extern RC deleteTable (char *name){
	RM_TableData * table = NULL;
	RC rc = searchTableP(&table, name);
	if(rc != RC_OK)
		return rc;

	if(table->mgmtData!=NULL){
		rc = closeTable(table);
		if(rc != RC_OK)
			return rc;
	}
	
	rc = removeTable(table);
	if(rc != RC_OK)
		return rc;
	
	destroyPageFile(name);
	free(table);
	return RC_OK;
}
extern int getNumTuples (RM_TableData *rel){
	return rel->numberOfTuples;
}

// handling records in a table
extern RC insertRecord (RM_TableData *rel, Record *record){
	BM_PageHandle *h = MAKE_PAGE_HANDLE();
	//if freepage list is empty
	int recordSize = getRecordSize(rel->schema);
	int pageNum = (int)(rel->numberOfTuples*recordSize/PAGE_SIZE);
        record->id.page=pageNum;
        int tuplesPerPage = PAGE_SIZE/recordSize;
        record->id.slot=rel->numberOfTuples%tuplesPerPage;

	BM_BufferPool *bm = rel->mgmtData;
	RC rc = pinPage(bm, h, pageNum);
	if(rc != RC_OK) 
		return rc;
	char * page = h->data;

	int offset = rel->numberOfTuples*recordSize;
	page= page+offset;
	if(offset >= PAGE_SIZE)
			return RC_INVALID_RID;
	sprintf(page, "%s", record->data);
	//Write record. sprintf? memcpy? strcpy?
	//Take into account the slot number of RID
	rc = markDirty(bm, h);
	if(rc != RC_OK){
		free(h);
		return rc;
	}
	rc = unpinPage(bm, h);
	rc = forcePage(bm, h);
	free(h);
	if(rc != RC_OK)
		return rc;
	if(record->id.page == 0 && record->id.slot == 0){
		rel->first = record;

	}
	rel->numberOfTuples++;
	return RC_OK;
}
extern RC deleteRecord (RM_TableData *rel, RID id){
	//add RID to list of emptySlots
	//account for if the first available record has been deleted

	BM_PageHandle *h = MAKE_PAGE_HANDLE();
        BM_BufferPool *bm = rel->mgmtData;
	int recordSize = getRecordSize(rel->schema);
        RC rc = pinPage(bm, h, id.page);
        if(rc != RC_OK)
                return rc;
        
        char * page = h->data;
        int offset = id.slot*recordSize;
	page+=offset;
        if(offset >= PAGE_SIZE)
                return RC_INVALID_RID;
        sprintf(page, "TOMB");
        //Write record. sprintf? memcpy? strcpy?
        //Take into account the slot number of RID
        rc = markDirty(bm, h);
        if(rc != RC_OK){
                free(h);
                return rc;
        }
        rc = unpinPage(bm, h);
        free(h);
        if(rc != RC_OK)
                return rc;

	rel->numberOfTuples--;
	return RC_OK;
}
extern RC updateRecord (RM_TableData *rel, Record *record){
	BM_PageHandle *h= MAKE_PAGE_HANDLE();
	BM_BufferPool *bm = rel->mgmtData;
	int recordSize = getRecordSize(rel->schema);
	RC rc = pinPage(bm, h, record->id.page);
	if(rc != RC_OK)
		return rc;

        char * page = h->data;
        int offset = record->id.slot*recordSize;
	page+=offset;
        if(offset >= PAGE_SIZE)
                return RC_INVALID_RID;
        sprintf(page, "%s", record->data);
        //Write record. sprintf? memcpy? strcpy?
        //Take into account the slot number of RID
        rc = markDirty(bm, h);
        if(rc != RC_OK){
                free(h);
                return rc;
        }
        rc = unpinPage(bm, h);
        free(h);
        if(rc != RC_OK)
                return rc;

	//update page with new record->data
	rc = markDirty(bm, h);
	if(rc != RC_OK){
		free(h);
		return rc;
	}
	rc = unpinPage(bm, h);
	free(h);
	if(rc != RC_OK)
		return rc;
	return RC_OK;
}
extern RC getRecord (RM_TableData *rel, RID id, Record *record){
	//validate that rid is valid
	int numberOfPagesInUse = (int)rel->numberOfTuples/PAGE_SIZE + 1;
	if(id.page>=numberOfPagesInUse)
		return RC_INVALID_RID;
	int recordSize = getRecordSize(rel->schema);
	int maxNumberOfTuples = (PAGE_SIZE/recordSize)*id.page + id.slot;
	if(maxNumberOfTuples > rel->numberOfTuples)
		return RC_INVALID_RID;

	BM_PageHandle *h= MAKE_PAGE_HANDLE();
	RC rc = pinPage(rel->mgmtData, h, id.page);
	if(rc != RC_OK)
		return rc;
	//populate record
	char * page = h->data;
	int offset = id.slot*recordSize;
	page+=offset;

	if(offset >= PAGE_SIZE)
		return RC_INVALID_RID;
	//memcpy instead of sprintf
	//copy data from disk using id into record->data
	record->id = id;
	int i;
	for(i = 0; i < recordSize; i++){
		record->data[i] = page[i];
	}
	//memcpy(record->data, page, recordSize);
	//cleanup
	rc = unpinPage(rel->mgmtData, h);
	free(h);
	if(rc != RC_OK)
		return rc;
	return RC_OK;
}

// scans
extern RC startScan (RM_TableData *rel, RM_ScanHandle *scan, Expr *cond){

	//populate scan object
	scan->rel = rel;
	scan->cond = cond;
	scan->scannedTuples = 0;
	scan->mgmtData = (Record *)malloc(sizeof(Record));
	if(rel->name == NULL)
		return RC_INVALID_REL;
	scan->mgmtData = rel->first;
	//poplate scan object with first record of rel
	//How to keep track of the first record of rel?
	return RC_OK;
}
extern RC next (RM_ScanHandle *scan, Record *record){
	int recordSize = getRecordSize(scan->rel->schema);
	int pageNum =(int)( (scan->scannedTuples*recordSize)/PAGE_SIZE);
	int slotsPerPage = (int)(PAGE_SIZE/recordSize);
	int slotNum = (scan->scannedTuples)%slotsPerPage;

	RID id;
	id.page = pageNum;
	id.slot = slotNum;

	getRecord(scan->rel, id, record);

	scan->scannedTuples++;
	if(scan->scannedTuples > scan->rel->numberOfTuples)
                return RC_RM_NO_MORE_TUPLES;
	if(scan->rel->name == NULL)
		return RC_INVALID_REL;
	if(scan->cond != NULL){
		//checkcondition
	}

	return RC_OK;
}
extern RC closeScan (RM_ScanHandle *scan){
	free(scan->mgmtData);
	return RC_OK;
}

	// dealing with schemas
extern int getRecordSize (Schema *schema){
	int i;
	int recordSize = 0;//Accounts for null terminator
	for(i = 0; i < schema->numAttr; i++){
		switch(schema->dataTypes[i]){
			case DT_INT:
				recordSize+=4;//Max number of digits 
				break;
			case DT_FLOAT:
				recordSize+=4;//Max number of digit + 1
				break;
			case DT_STRING:
				recordSize+=20;
				break;
			case DT_BOOL:
				recordSize++;
				break;
			default: 
				return -1;
		}
	}
	return recordSize;
}
extern Schema *createSchema (int numAttr, char **attrNames, DataType *dataTypes, int *typeLength, int keySize, int *keys){
	Schema * schema = (Schema *)malloc(sizeof(Schema));
	schema->numAttr = numAttr;
	schema->attrNames = attrNames;
	schema->dataTypes = dataTypes;
	schema->typeLength = typeLength;
	schema->keySize = keySize;
	schema->keyAttrs = keys;
	return schema;
}
extern RC freeSchema (Schema *schema){
	free(schema->keyAttrs);
	free(schema->typeLength);
	free(schema->dataTypes);
	int i;
	for(i = 0; i < schema->numAttr; i++){
		free(schema->attrNames[i]);
	}
	free(schema->attrNames);
	return RC_OK;
}

// dealing with records and attribute values
extern RC createRecord (Record **record, Schema *schema){
	*record = malloc(sizeof(Record));
	int recordSize = getRecordSize(schema);
	(*record)->data = calloc(recordSize, sizeof(char));
	return RC_OK;
}
extern RC freeRecord (Record *record){
	free(record->data);
	free(record);
	return RC_OK;
}
extern RC getAttr (Record *record, Schema *schema, int attrNum, Value **value, int *offset){
	RC rc= RC_OK; 
	char * data = record->data;
	*value = malloc(sizeof(Value));
	DataType type = schema->dataTypes[attrNum];
	if(type == DT_INT){
			(*value)->dt = DT_INT;
			char str[] = "\0\0\0\0"; 
			char * temp = data + *offset;
			int i;
			int index = 0;
			bool leadingZeroes = true;
			for(i = 0; i < 4; i++){
				if(temp[i] != '0' || !leadingZeroes){
					leadingZeroes = false; 
					str[index] = temp[i];
					index++;
				}
			}
			
			int val = (int) strtol(str, (char **)NULL, 10);
			(*value)->v.intV = val;
			*offset+=4;
	}
	else if(type == DT_STRING){
			(*value)->dt = DT_STRING;
                        char str[] = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"; 
                        char * temp = data + *offset;
                        int i;
			int index = 0;
			bool leadingSpaces = true;
                        for(i = 0; i < 20; i++){ 
				if(temp[i] != ' ' || !leadingSpaces){
					leadingSpaces = false;
                                	str[index] = temp[i];
					index++;
				}
                        }
		        (*value)->v.stringV = str;
			*offset+=20;
	}
	else if(type == DT_FLOAT){
			(*value)->dt = DT_FLOAT;
                        char str[4]; 
                        char * temp = data + *offset;
                        int i;
                        for(i = 0; i < 4; i++){
                                str[i] = temp[i];
                        }
                        (*value)->v.floatV =  strtof(str, NULL);
                        *offset+=4;
	}
	else{
			(*value)->dt = DT_BOOL; 
                        char * temp = data + *offset;
                        int i;
                        if(*temp == 'T')
				(*value)->v.boolV = true;
			else if(*temp == 'F')
				(*value)->v.boolV = false;
			else

				rc = RC_VALUE_ASSIGNMENT_FAILED; 
                        *offset++;

	}
	return rc;
}
extern RC setAttr (Record *record, Schema *schema, int attrNum, Value *value, int * offset){
        RC rc = RC_OK;
	char * data = record->data;
	char * temp = data + *offset;
        DataType type = schema->dataTypes[attrNum];
	int i;
	if(type == DT_INT){
                char str[4];
                sprintf(str, "%i", value->v.intV);
		int strlength = strlen(str);
		for(i = 0; i < 4; i++){
			if(i >= (4-strlength))
                        	temp[i] = str[i-(4-strlength)];
			else
				temp[i] = '0';
                } 
		(*offset)+=4;
	}
        else if(type == DT_STRING){
		char str[20];
		sprintf(str, "%s", value->v.stringV);
		int strlength = strlen(str);
		for(i = 0; i < 20; i++){
			if(i >= (20-strlength))
                        	temp[i] = str[i-(20-strlength)];
			else
				temp[i] = ' ';
                }  
		(*offset)+=20;
	}
        else if(type == DT_FLOAT){
		char str[4];
		sprintf(str, "%d", value->v.floatV);
		int strlength = strlen(str);
		for(i = 0; i < 4; i++){
			if(i >= strlength)
				temp[i] = '0';
                	else
				temp[i] = str[i];
                }         
		(*offset)+=4;
	}
        else{
		if(value->v.boolV == true)
			*temp = 'T';
		else if(value->v.boolV == false)
			*temp = 'F';
		else rc = RC_VALUE_ASSIGNMENT_FAILED;
        }
	return rc;
}  
