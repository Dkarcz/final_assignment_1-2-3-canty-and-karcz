#include "dberror.h"
#include "storage_mgr.h"
#include "buffer_mgr.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

//Global Variables
//Test
int pageFrameID;

RC searchPage(BM_BufferPool * const bm, BM_PageHandle * page, BM_PageFrame **pageFramePointer){
	//This algorithm starts at the begninning of the Page Frame List
	BM_PageFrame * current = bm->head;
	//Search algorithm takes O(n) where n is the number of pages
	while(current != bm->tail){
		if(current->page->pageNum == page->pageNum){
			break;
		}
		current = current->next;
	}
	if(current == bm->tail && current->page->pageNum != page->pageNum)
		return RC_PAGE_NOT_FOUND;
	*pageFramePointer = current;
	return RC_PAGE_FOUND;
}
RC appendPage(BM_BufferPool *const bm, BM_PageHandle *const page, PageNumber const pageNum){
	//Should only be called if the PageList has capacity
	//This search is redundant
	//Remove Empty Frame from position
	
        BM_PageFrame * current = bm->head;
        
	int i;
        
        for(i = 0; i < bm->capacity; i++){
                if(current->referenceCount == 0)
                        break;
                current = current->next;
        }
        if(i == bm->capacity)
                return RC_REPLACE_FAILED;
	
	if(i == 0){//Head of list being replaced
		bm->head = bm->head->next;
	}
	else if(i!=(bm->capacity-1)){//Frame being replaced is not the tail
		current->next->previous = current->previous;
        	current->previous->next = current->next;
		bm->tail->next = current;
		current->previous = bm->tail;
		current->next = bm->head;
		bm->head->previous = current;
	}
	bm->tail = current;
        //Opening file. bm->mgmtData is the SM_FileHandle
        RC rc = openPageFile((char *)bm->pageFile, bm->mgmtData);
        if(rc != RC_OK)
                return rc;

	//Populate Frame
	SM_PageHandle data = (SM_PageHandle)malloc(PAGE_SIZE*sizeof(char));;
	rc = readBlock(pageNum, bm->mgmtData, data);
	//Not sure whether to proceed or stop if rc != RC_OK
	//If pageNum is invalid data should point to empty page of memory
	if(rc != RC_OK && rc != RC_READ_NON_EXISTING_PAGE)
		return rc;
	if(rc == RC_OK)
		bm->numReadIO++;

	//Close file
        rc = closePageFile(bm->mgmtData);
        if(rc != RC_OK)
                return rc;
	current->page->pageNum = pageNum;
	current->page->data = (char *)data;
	page->data = data;	
	current->referenceCount = 1;
	current->dirtyBit = false;
	
	//Decrease Capacity
	bm->numPages--;
	
	return RC_OK;
}
RC foundPageLRU(BM_BufferPool *const bm, BM_PageFrame *const current){
	//Should only be done with a valid pageFrame pointer
        if(current == bm->head){//Head of list being replaced
                bm->head = bm->head->next;
        }
        else if(current != bm->tail){//Frame that is not the last being replaced
                current->next->previous = current->previous;
                current->previous->next = current->next;
                bm->tail->next = current;
                current->previous = bm->tail;
                current->next = bm->head;
                bm->head->previous = current;
        }
	else{
		return RC_OK;
	}
	bm->tail = current;
	return RC_OK;
}
RC evictPage(BM_BufferPool *const bm, BM_PageFrame *const  pageFrame){
	//Should only be called with a valid pageFrame
	RC rc = -99;
	//Should only proceed of referenceCount is 0
	if(pageFrame->referenceCount!=0)
		return RC_EVICT_FAILED;
	//Does the order between these two if statements matter?
	//Write to disk if dirty
	if(pageFrame->dirtyBit)
		rc = forcePage(bm, pageFrame->page);
	if(rc != RC_OK && rc != -99)
		return rc;

	//Reset pageFrame attributes
	pageFrame->page->pageNum = NO_PAGE;
	free(pageFrame->page->data);
	pageFrame->page->data = NULL;
        pageFrame->dirtyBit = false;

	//Increase Capacity
	bm->numPages++;
	return RC_OK;
}
RC replaceFIFO(BM_BufferPool *const bm, BM_PageHandle *const page, const PageNumber pageNum){
	RC rc;
	BM_PageFrame * current = bm->head;
	int i;
	
	for(i = 0; i < bm->capacity; i++){
		if(current->referenceCount == 0)
			break;
		current = current->next;
	}
	if(i == bm->capacity)
		return RC_REPLACE_FAILED;
	
	rc = evictPage(bm, current);
	if(rc != RC_OK)
		return rc;
	
	rc = appendPage(bm, page, pageNum);
	if(rc != RC_OK)
		return rc;

	return RC_OK;
}

// Buffer Manager Interface Pool Handling
RC initBufferPool(BM_BufferPool *const bm, const char *const pageFileName, 
		const int numPages, ReplacementStrategy strategy,
		void *stratData){
	//Invalid Input
	if(numPages <= 0)
		return RC_INVALID_NUMPAGES;
	
	//Init BufferPool
	bm->pageFile = pageFileName;
	bm->numPages = numPages;
	bm->capacity = numPages;
	bm->strategy = strategy;
	bm->numReadIO = 0;
	bm->numWriteIO = 0;
	bm->mgmtData = (SM_FileHandle *)malloc(sizeof(SM_FileHandle)); 
	pageFrameID = 0;
	
	//Init PageFram List
	int i;
	BM_PageFrame * listOfPageFrames[numPages];
	for(i = 0; i < numPages; i++){
		listOfPageFrames[i] = (BM_PageFrame *)malloc(sizeof(BM_PageFrame));
		listOfPageFrames[i]->page = MAKE_PAGE_HANDLE();
		listOfPageFrames[i]->page->pageNum = NO_PAGE;
		listOfPageFrames[i]->page->data = NULL;
		listOfPageFrames[i]->pageFrameNum = i;
		listOfPageFrames[i]->referenceCount = 0;
		listOfPageFrames[i]->dirtyBit = false;
		if(i != 0){
			listOfPageFrames[i]->previous = listOfPageFrames[i-1];
			listOfPageFrames[i-1]->next = listOfPageFrames[i];
		}
	}
	bm->head = listOfPageFrames[0];
	bm->tail = listOfPageFrames[--i];
	bm->head->previous = bm->tail;
	bm->tail->next = bm->head;
	//Init Page Frame List

	return RC_OK;
}
RC shutdownBufferPool(BM_BufferPool *const bm){
	//First we must account for dirty pages
	RC rc = forceFlushPool(bm);

	//This algorithim for freeing memory starts at the tail
	int i;
	BM_PageFrame * current = bm->tail;

	//Terminate once the current pointer reaches the head of the list
        while(current != bm->head){
		current = current->previous;
		if(current->next->page->pageNum != NO_PAGE)
			free(current->next->page->data);
		free(current->next->page);
		free(current->next);
        }

	//Free head
	free(bm->head->page->data);
	free(bm->head->page);
	free(bm->head);

	//Free SM_FileHandle
	free(bm->mgmtData);
	return RC_OK;
}
RC forceFlushPool(BM_BufferPool *const bm){
	//This algorithm terminates once it reaches the tail of the list
	RC rc;
	BM_PageFrame * current = bm->head;
	while(current != bm->tail){
		if(current->dirtyBit == true && current->referenceCount == 0){
			rc = forcePage(bm, current->page);
			if(rc != RC_OK)
				return rc;
		}
		current = current->next;
	}
	if(bm->tail->dirtyBit == true && bm->tail->referenceCount == 0){
		rc = forcePage(bm, current->page);
		if(rc != RC_OK)
			return rc;
	}
	
	return RC_OK;
}

// Buffer Manager Interface Access Pages
RC markDirty (BM_BufferPool *const bm, BM_PageHandle *const page){
	//Before performing task, ensure that the page exists within the PageFrameList
	BM_PageFrame * pageFrame = NULL;
        RC rc = searchPage(bm, page, &pageFrame);
	
	if(rc != RC_PAGE_FOUND)
		return rc;

	pageFrame->dirtyBit = true;
       
	return RC_OK;
}
RC unpinPage (BM_BufferPool *const bm, BM_PageHandle *const page){
	//Before performing task, ensure that the page exists within the PageFrameList
	BM_PageFrame * pageFrame = NULL;
	RC rc = searchPage(bm, page, &pageFrame);
	//Return error if not found
	if(rc != RC_PAGE_FOUND)
		return rc;
	//Decrement pageFrame reference count
	pageFrame->referenceCount--;
	if(pageFrame->referenceCount < 0)
		pageFrame->referenceCount = 0;

	return RC_OK;
}
RC forcePage (BM_BufferPool *const bm, BM_PageHandle *const page){
	//Before performing task, ensure that the page exists within the PageFrameList
	BM_PageFrame * pageFrame = NULL;
        RC rc = searchPage(bm, page, &pageFrame);
	if(rc != RC_PAGE_FOUND)
		return rc; 
	
	//Opening file bm->mgmtData is the SM_FileHandle
	rc = openPageFile((char *)bm->pageFile, bm->mgmtData);
	if(rc != RC_OK)
		return rc;
	//Write page to file, ensure pages first
	rc = ensureCapacity(page->pageNum+1, bm->mgmtData, NULL);
	if(rc != RC_OK)
		return rc;
	rc = writeBlock(page->pageNum, bm->mgmtData, page->data);
	if(rc != RC_OK)
		return rc;
	//Close file
	rc = closePageFile(bm->mgmtData);
	if(rc != RC_OK)
		return rc;

	bm->numWriteIO++;
	if(pageFrame->dirtyBit)
		pageFrame->dirtyBit = false;

	return RC_OK;
}
RC pinPage (BM_BufferPool *const bm, BM_PageHandle *const page, 
		const PageNumber pageNum){
	//Before perfomring task, ensure that the page does not exist within the PageFrameList
	BM_PageFrame * pageFrame = NULL;
	page->pageNum = pageNum;
        RC rc = searchPage(bm, page, &pageFrame);
	//Page already pinned
	
	if(rc == RC_PAGE_FOUND){
		switch(bm->strategy){
                        case RS_FIFO :
  	              		//INCREMENT REFERENCE COUNT??
        	        	//WHAT IF THE SAME CLIENT IS CONTINUOUSLY PINNING A PAGE?
                		pageFrame->referenceCount++;
                		return RC_OK;

                        case RS_LRU :
                                rc = foundPageLRU(bm, pageFrame);
				if(rc!= RC_OK)
					return rc;
				break;
                        default :
                                return RC_INVALID_STRATEGY;
                } 
	}
	if(bm->numPages > 0){
		rc = appendPage(bm, page, pageNum);
	}
	else{
		switch(bm->strategy){
			case RS_FIFO :
				rc = replaceFIFO(bm, page, pageNum);
				break;
			case RS_LRU :
				rc = replaceFIFO(bm, page, pageNum);
				break;
			default :
				return RC_INVALID_STRATEGY;
		}
	}
	if(rc != RC_OK)
		return rc;
	
	return RC_OK;
	
}

// Statistics Interface
PageNumber *getFrameContents (BM_BufferPool *const bm){
	PageNumber * pageNumbers = (PageNumber *)malloc(sizeof(PageNumber)*bm->capacity);
	BM_PageFrame* current = bm->head;
	int i;
	for(i = 0; i < bm->capacity; i++){
		pageNumbers[i] = current->page->pageNum;
		current = current->next;
	}
	return pageNumbers;
}
bool *getDirtyFlags (BM_BufferPool *const bm){
	bool * dirtyBits = (bool *)malloc(sizeof(bool)*bm->capacity);
	int i;
	BM_PageFrame* current = bm->head;
	for(i = 0; i < bm->capacity; i++){
		dirtyBits[i] = current->dirtyBit;
		current = current->next;
	}
	return dirtyBits;
}
int *getFixCounts (BM_BufferPool *const bm){
	int * referenceCounts = (int *)malloc(sizeof(int)*bm->capacity);
	int i;
	BM_PageFrame * current = bm->head;
	for(i = 0; i< bm->capacity; i++){
		referenceCounts[i] = current-> referenceCount;
		current = current->next;
	}
	return referenceCounts;
}
int getNumReadIO (BM_BufferPool *const bm){
	return bm->numReadIO;
}
int getNumWriteIO (BM_BufferPool *const bm){
        return bm->numWriteIO;
}
